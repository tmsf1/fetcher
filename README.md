# Fetcher

## Description
The application consists in 3 modules: 
 - HttpClient, responsible from fetching the website
 - HtmlParser, a module that uses :fast_html to parse string into objects
 - Fetcher with the  `fetch(url)` function as requested, alongside pretty printing 
 functions that will transform the raw output coming from the Html Parser  in the the request key value format.
 - For extensibility: there is a configuration function within Fetcher, where we can add in more html tags to use, 
 alongside the value associated with it that we want to display

## Considerations 
- No error handling for http client or for html parser were added
- `%{:html_tag => element, :tags => tags, :inner_html => inner_html}` could've been set as a struct `%ParsedHtml{:html_tag, :tags, :inner_html}`

### Usage example

```sh
iex -S mix
```

```elixir
Fetcher.fetch("https://xkcd.com/2317/")
```

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `fetcher` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:fetcher, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/fetcher](https://hexdocs.pm/fetcher).

