defmodule FetcherTest do
  use ExUnit.Case
  doctest Fetcher

  import Mox

  test "if there is an http error fetch will return nil" do
    HttpClient
    |> expect(:get_as_string, fn _x -> {:error, "fake-error"} end)

    assert Fetcher.fetch('some_url') == nil
  end

  test "when http client succeeds it will return a succesfully parsed website" do
    HttpClient
    |> expect(:get_as_string, fn _x -> {:ok, mock_website_string()} end)

    assert Fetcher.fetch('some_url') == expected_parsed_website()
  end

  test "an 'empty' website will return empty mapping" do
    HttpClient
    |> expect(:get_as_string, fn _x -> {:ok, ""} end)

    assert Fetcher.fetch('some_url') == %{assets: '', links: []}
  end

  test "prettifying of raw parsed content should return the apropriate tag by html tag" do
    assert Fetcher.prettify(mock_xkcd(), %{
             :links => %{:html_element => "a", :tag => "href"},
             :assets => %{:html_element => "img", :tag => "src"}
           }) == %{
             assets: [
               "/s/0b7742.png",
               "//imgs.xkcd.com/news/blm.png",
               "//imgs.xkcd.com/comics/pinouts.png",
               "//imgs.xkcd.com/s/a899e84.jpg",
               "//imgs.xkcd.com/s/temperature.png"
             ],
             links: [
               "/archive",
               "http://what-if.xkcd.com",
               "http://blag.xkcd.com",
               "/how-to/",
               "http://store.xkcd.com/",
               "/about",
               "/atom.xml",
               "/newsletter/",
               "/",
               "https://www.joincampaignzero.org/",
               "/1/",
               "/2316/",
               "//c.xkcd.com/random/comic/",
               "/2318/",
               "/",
               "/1/",
               "/2316/",
               "//c.xkcd.com/random/comic/",
               "/2318/",
               "/",
               "//xkcd.com/1732/",
               "/rss.xml",
               "/atom.xml",
               "/newsletter/",
               "http://threewordphrase.com/",
               "http://www.smbc-comics.com/",
               "http://www.qwantz.com",
               "http://oglaf.com/",
               "http://www.asofterworld.com",
               "http://buttersafe.com/",
               "http://pbfcomics.com/",
               "http://questionablecontent.net/",
               "http://www.buttercupfestival.com/",
               "http://www.mspaintadventures.com/?s=6&p=001901",
               "http://www.jspowerhour.com/",
               "https://medium.com/civic-tech-thoughts-from-joshdata/so-you-want-to-reform-democracy-7f3b1ef10597",
               "https://www.nytimes.com/interactive/2017/climate/what-is-climate-change.html",
               "https://twitter.com/KHayhoe",
               "http://creativecommons.org/licenses/by-nc/2.5/",
               "/license.html"
             ]
           }
  end

  test "prettifying of raw parsed content without images and with links without href should return the apropriate tag by html tag" do
    assert Fetcher.prettify(mock_parsed_content(), %{
             :links => %{:html_element => "a", :tag => "href"},
             :assets => %{:html_element => "img", :tag => "src"}
           }) == expected_parsed_website()
  end

  def mock_xkcd() do
    [
      %{html_element: "a", tags: [{"href", "/archive"}], visible_string: ["Archive"]},
      %{
        html_element: "a",
        tags: [{"href", "http://what-if.xkcd.com"}],
        visible_string: ["What If?"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://blag.xkcd.com"}],
        visible_string: ["Blag"]
      },
      %{html_element: "a", tags: [{"href", "/how-to/"}], visible_string: ["How To"]},
      %{
        html_element: "a",
        tags: [{"href", "http://store.xkcd.com/"}],
        visible_string: ["Store"]
      },
      %{
        html_element: "a",
        tags: [{"rel", "author"}, {"href", "/about"}],
        visible_string: ["About"]
      },
      %{html_element: "a", tags: [{"href", "/atom.xml"}], visible_string: ["Feed"]},
      %{html_element: "a", tags: [{"href", "/newsletter/"}], visible_string: ["Email"]},
      %{
        html_element: "a",
        tags: [{"href", "/"}],
        visible_string: [
          {"img",
           [
             {"src", "/s/0b7742.png"},
             {"alt", "xkcd.com logo"},
             {"height", "83"},
             {"width", "185"}
           ], []}
        ]
      },
      %{
        html_element: "img",
        tags: [
          {"src", "/s/0b7742.png"},
          {"alt", "xkcd.com logo"},
          {"height", "83"},
          {"width", "185"}
        ]
      },
      %{
        html_element: "a",
        tags: [{"href", "https://www.joincampaignzero.org/"}],
        visible_string: [
          {"img", [{"border", "0"}, {"src", "//imgs.xkcd.com/news/blm.png"}], []}
        ]
      },
      %{
        html_element: "img",
        tags: [{"border", "0"}, {"src", "//imgs.xkcd.com/news/blm.png"}]
      },
      %{html_element: "a", tags: [{"href", "/1/"}], visible_string: ["|<"]},
      %{
        html_element: "a",
        tags: [{"rel", "prev"}, {"href", "/2316/"}, {"accesskey", "p"}],
        visible_string: ["< Prev"]
      },
      %{
        html_element: "a",
        tags: [{"href", "//c.xkcd.com/random/comic/"}],
        visible_string: ["Random"]
      },
      %{
        html_element: "a",
        tags: [{"rel", "next"}, {"href", "/2318/"}, {"accesskey", "n"}],
        visible_string: ["Next >"]
      },
      %{html_element: "a", tags: [{"href", "/"}], visible_string: [">|"]},
      %{
        html_element: "img",
        tags: [
          {"src", "//imgs.xkcd.com/comics/pinouts.png"},
          {"title",
           "The other side of USB-C is rotationally symmetric except that the 3rd pin from the top is designated FIREWIRE TRIBUTE PIN."},
          {"alt", "Pinouts"},
          {"srcset", "//imgs.xkcd.com/comics/pinouts_2x.png 2x"}
        ]
      },
      %{html_element: "a", tags: [{"href", "/1/"}], visible_string: ["|<"]},
      %{
        html_element: "a",
        tags: [{"rel", "prev"}, {"href", "/2316/"}, {"accesskey", "p"}],
        visible_string: ["< Prev"]
      },
      %{
        html_element: "a",
        tags: [{"href", "//c.xkcd.com/random/comic/"}],
        visible_string: ["Random"]
      },
      %{
        html_element: "a",
        tags: [{"rel", "next"}, {"href", "/2318/"}, {"accesskey", "n"}],
        visible_string: ["Next >"]
      },
      %{html_element: "a", tags: [{"href", "/"}], visible_string: [">|"]},
      %{
        html_element: "img",
        tags: [
          {"src", "//imgs.xkcd.com/s/a899e84.jpg"},
          {"width", "520"},
          {"height", "100"},
          {"alt", "Selected Comics"},
          {"usemap", "#comicmap"}
        ]
      },
      %{
        html_element: "a",
        tags: [{"href", "//xkcd.com/1732/"}],
        visible_string: [
          {"img",
           [
             {"border", "0"},
             {"src", "//imgs.xkcd.com/s/temperature.png"},
             {"width", "520"},
             {"height", "100"},
             {"alt", "Earth temperature timeline"}
           ], []}
        ]
      },
      %{
        html_element: "img",
        tags: [
          {"border", "0"},
          {"src", "//imgs.xkcd.com/s/temperature.png"},
          {"width", "520"},
          {"height", "100"},
          {"alt", "Earth temperature timeline"}
        ]
      },
      %{html_element: "a", tags: [{"href", "/rss.xml"}], visible_string: ["RSS Feed"]},
      %{html_element: "a", tags: [{"href", "/atom.xml"}], visible_string: ["Atom Feed"]},
      %{html_element: "a", tags: [{"href", "/newsletter/"}], visible_string: ["Email"]},
      %{
        html_element: "a",
        tags: [{"href", "http://threewordphrase.com/"}],
        visible_string: ["Three Word Phrase"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://www.smbc-comics.com/"}],
        visible_string: ["SMBC"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://www.qwantz.com"}],
        visible_string: ["Dinosaur Comics"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://oglaf.com/"}],
        visible_string: ["Oglaf"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://www.asofterworld.com"}],
        visible_string: ["A Softer World"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://buttersafe.com/"}],
        visible_string: ["Buttersafe"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://pbfcomics.com/"}],
        visible_string: ["Perry Bible Fellowship"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://questionablecontent.net/"}],
        visible_string: ["Questionable Content"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://www.buttercupfestival.com/"}],
        visible_string: ["Buttercup Festival"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://www.mspaintadventures.com/?s=6&p=001901"}],
        visible_string: ["Homestuck"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://www.jspowerhour.com/"}],
        visible_string: ["Junior Scientist Power Hour"]
      },
      %{
        html_element: "a",
        tags: [
          {"href",
           "https://medium.com/civic-tech-thoughts-from-joshdata/so-you-want-to-reform-democracy-7f3b1ef10597"}
        ],
        visible_string: ["Tips on technology and government"]
      },
      %{
        html_element: "a",
        tags: [
          {"href", "https://www.nytimes.com/interactive/2017/climate/what-is-climate-change.html"}
        ],
        visible_string: ["Climate FAQ"]
      },
      %{
        html_element: "a",
        tags: [{"href", "https://twitter.com/KHayhoe"}],
        visible_string: ["Katharine Hayhoe"]
      },
      %{
        html_element: "a",
        tags: [{"href", "http://creativecommons.org/licenses/by-nc/2.5/"}],
        visible_string: ["Creative Commons Attribution-NonCommercial 2.5 License"]
      },
      %{
        html_element: "a",
        tags: [{"rel", "license"}, {"href", "/license.html"}],
        visible_string: ["More details"]
      }
    ]
  end

  def mock_parsed_content() do
    [
      %{
        html_element: "a",
        tags: [{"onclick", "toggle_visibility('about');"}],
        visible_string: ["about"]
      },
      %{
        html_element: "a",
        tags: [
          {"href", "https://babelpuntodeencuentro.org/"},
          {"target", "_blank"},
          {"rel", "noopener"}
        ],
        visible_string: [" Babel Punto de Encuentro "]
      },
      %{
        html_element: "a",
        tags: [{"onclick", "toggle_visibility('bio');"}],
        visible_string: ["bio"]
      },
      %{
        html_element: "a",
        tags: [
          {"href", "https://hdl.handle.net/1822/27820"},
          {"target", "_blank"},
          {"rel", "noopener"}
        ],
        visible_string: [" weak secret key-agreement protocols"]
      },
      %{
        html_element: "a",
        tags: [
          {"href", "/cdn-cgi/l/email-protection#6c04090000032c18050d0b0342180a"},
          {"target", "_blank"},
          {"rel", "noopener"}
        ],
        visible_string: ["mail"]
      },
      %{
        html_element: "a",
        tags: [
          {"href", "https://github.com/tmsf"},
          {"target", "_blank"},
          {"rel", "noopener"}
        ],
        visible_string: ["github"]
      },
      %{
        html_element: "a",
        tags: [
          {"href", "https://twitter.com/schroder"},
          {"target", "_blank"},
          {"rel", "noopener"}
        ],
        visible_string: ["twitter"]
      },
      %{
        html_element: "a",
        tags: [
          {"href", "https://www.linkedin.com/in/tmsfernandes"},
          {"target", "_blank"},
          {"rel", "noopener"}
        ],
        visible_string: ["linkedin"]
      }
    ]
  end

  defp mock_website_string() do
    "<meta name=\"description\" content=\"home page\">\n<meta name=\"author\" content=\"Tiago Fernandes\">\n<meta name=\"viewport\" content=\"initial-scale = 1.0, maximum-scale = 5.0\" />\n<link rel='stylesheet' href='/assets/styles/main.css' type='text/css'>\n<html lang=\"en\">\n<head>\n<title>Tiago Fernandes</title>\n</head>\n<main>\n<section>\n<h1 class='title font-smoothing'>Tiago Fernandes</h1>\n<h2 class='subtitle font-smoothing'>software developer</h2>\n</section>\n<section class=\"module\">\n<div class=\"navcontainer\">\n<ul>\n<li>\n<a onclick=\"toggle_visibility('about');\">about</a>\n<div class=\"container\" id=\"about\" style='display:none'>\n<div>Since the beginning of my career I've been doing software\ndevelopment in a myriad of technologies, such as <em>node/javascript</em>,\n<em>ruby on rails</em>, <em>elixir/erlang</em>, <em>rust</em>,\n<em>.NET MVC</em>, and <em>php</em>.\n</div>\n<div>I volunteer at\n<a href=\"https://babelpuntodeencuentro.org/\" target=_blank rel=\"noopener\"> Babel Punto de Encuentro </a>\nas web developer, a small but energetic Barcelona based NGO.\nThey run a school in Senegal's capital Dakar, and do some great\ncommunity work in Barcelona as well, providing counselling and\npsychological help to people who are in a vulnerable situation.\n</div>\n</div>\n</li>\n<li>\n<a onclick=\"toggle_visibility('bio');\">bio</a>\n<div class=\"container\" id=\"bio\" style='display:none'>\n<div> Portuguese currently living in Barcelona. I have a MSc. in Computer Science,\nwith specialization on Artificial Inteligence and Cryptography.\nMy dissertation's subject was <a href=\"https://hdl.handle.net/1822/27820\" target=_blank rel=\"noopener\"> weak secret key-agreement protocols</a>.\n</div>\n</div>\n</li>\n<li><a href=\"/cdn-cgi/l/email-protection#6c04090000032c18050d0b0342180a\" target='_blank' rel=\"noopener\">mail</a></li>\n<li><a href='https://github.com/tmsf' target='_blank' rel=\"noopener\">github</a></li>\n<li><a href='https://twitter.com/schroder' target='_blank' rel=\"noopener\">twitter</a></li>\n<li><a href='https://www.linkedin.com/in/tmsfernandes' target='_blank' rel=\"noopener\">linkedin</a></li>\n</ul>\n</div>\n</section>\n<section>\n<div class='copyright'>\ncopyright © 2016-2020, Tiago Fernandes\n</div>\n</section>\n</main>\n<script data-cfasync=\"false\" src=\"/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js\"></script></html>\n<script type=\"text/javascript\">\n    function toggle_visibility(id) {\n       const e = document.getElementById(id);\n       if(e.style.display == 'block')\n          e.style.display = 'none';\n       else\n          e.style.display = 'block';\n    }\n</script>\n"
  end

  defp expected_parsed_website() do
    %{
      assets: [],
      links: [
        "https://babelpuntodeencuentro.org/",
        "https://hdl.handle.net/1822/27820",
        "/cdn-cgi/l/email-protection#6c04090000032c18050d0b0342180a",
        "https://github.com/tmsf",
        "https://twitter.com/schroder",
        "https://www.linkedin.com/in/tmsfernandes"
      ]
    }
  end
end
