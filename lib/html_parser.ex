defmodule HtmlParser do
  import Logger

  @doc """
    given a string with the `html_string`
    it will return a list of maps of html tag elements
  """
  def parse_website_from_string(html_string) do
    # any string will be parsed by :fast_html.decode
    # and will return a valid fast_html data structure output
    {:ok, parsed_body} = :fast_html.decode(html_string, [])

    extract(parsed_body)
    |> Enum.map(fn e -> tuple_to_map(e) end)
  end

  @doc """
    extracts all the ocurrences of the html elements as a list of objects
  """
  def extract([]) do
    []
  end

  # elements might have inner html, so in order to parse the whole page
  # we need to perform extract in all the elements of a list of fast_html elements
  # and also on each of the element's inner html
  def extract([elem = {_element, _tags, inner_html} | tail]) do
    [elem] ++ extract(inner_html) ++ extract(tail)
  end

  # skip parsing of free text
  # e.g. javascript scripts and "\n"
  def extract([some_string | tail]) when is_binary(some_string) do
    extract(tail)
  end

  # skip parsing of :fast_html reserved fields (e.g. :comment)
  def extract([some_tuple | tail]) when is_tuple(some_tuple) do
    extract(tail)
  end

  # Catch all clause. Intended for debugging any potential edge case not considered
  # as I'm not entirely familiar with fast_html
  def extract([some_element | tail]) do
    Logger.warn("Unmatched element")
    IO.inspect(some_element)
    extract(tail)
  end

  # from tuple to map
  defp tuple_to_map(_elem = {element, tags, inner_html}) do
    %{:html_element => element, :tags => tags, :inner_html => inner_html}
  end
end
