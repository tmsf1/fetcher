defmodule Fetcher do
  alias HtmlParser
  alias HttpClient
  require Logger

  @moduledoc """
  Documentation for `Fetcher`.
  """

  @doc """
   fetchs website based on an url and extracts
   html tag related data based on config (see config function)
  """
  def fetch(url) do
    Logger.info("fetching: #{url}")

    case HttpClient.get_as_string(url) do
      {:ok, html_as_string} -> parse(html_as_string)
      {:error, msg} -> log_error(msg)
    end
  end

  @doc """
   transforms the website string payload into the mapping
   of html tags based on config
  """
  def parse(html_as_string) do
    config = config()

    parsed =
      html_as_string
      |> HtmlParser.parse_website_from_string()
      |> prettify(config.pretty_print)
  end

  @doc """
    defines the config for Fetcher
    :pretty_print defines the association between the html elements, the associated tag to print, and the key where they will be displayed
  """
  def config() do
    %{
      :pretty_print => %{
        :links => %{:html_element => "a", :tag => "href"},
        :assets => %{:html_element => "img", :tag => "src"}
      }
    }
  end

  @doc """
    prettyfies the generic output of HtmlParser
    Given the raw parsed mappings, and pretty print configuration
    prettify will extract the appropriate properties associated with each entry of a html element:
    e.g.
    :links => %{ :html_element => "a", :tag => "href"}
    For all the occurrences of the `a` element, we will set all the value associated with the tag `href`
    in a list under the key :links
  """
  def prettify(parsed_content, pp_config) do
    # get list of keys to iterate on the multiple mappings
    Map.keys(pp_config)
    # iterate the content to transform it with the given pp_config
    |> Enum.map(fn key -> prettify_by_key(parsed_content, key, pp_config[key]) end)
    # merge list of maps into map
    |> Enum.reduce(fn v, acc -> Map.merge(v, acc) end)
  end

  @doc """
    prettify_by_key will create the key value combination of the values for each occurrence of the html_tag
  """
  def prettify_by_key(parsed_content, key, pp_config = %{:html_element => html_element}) do
    list =
      Enum.filter(parsed_content, fn %{:html_element => ht} -> ht == html_element end)
      # get value of a particular tag
      |> Enum.map(fn a -> get_value_by_tag(a, pp_config) end)
      # removes entries where the tag searched for does not exist
      |> Enum.filter(fn x -> x !== nil end)

    %{key => list}
  end

  @doc """
    get_value_by_tag will find the value for the config tag in the list of tuples
    it will return nil if the tag is not present
  """
  def get_value_by_tag(%{:tags => tags}, _pp_config = %{:tag => config_tag}) do
    # return the value associated with tag, nil otherwise
    Enum.map(tags, fn {t, value} ->
      if(t == config_tag) do
        value
      end
    end)
    # remove all nil occurences from the list
    |> Enum.filter(fn x -> x !== nil end)
    # extracting the first element of the filtered list, if none found it will return nil
    |> Enum.at(0)
  end

  defp log_error(msg) do
    Logger.error(msg)
    nil
  end
end
