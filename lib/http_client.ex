defmodule HttpClient do
  @callback get_as_string(String.t()) :: {:atom, String.t()}
  @doc """
    returns a website body in string format
  """
  def get_as_string(url) do
    case HTTPoison.get(url, [], timeout: 50_000) do
      {:ok, response} -> {:ok, response.body}
      {:error, _} -> {:error, 'http error'}
    end
  end
end
